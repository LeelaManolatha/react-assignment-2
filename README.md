**React Assignment 2**

The project contains two Components:

- ***App.js*** fetches the members ids when the component is mounted and allows user to select any one of the id which turns on 'isDataPopulated' and the selected id is passed as a prop to the the *MemberDetails* component.
 

- ***MemberDetails.js*** recieves the selected member id and displays the details of the member along with the list of team members ids.