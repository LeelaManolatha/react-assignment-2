import React,{Component} from 'react';

import './App.css';
import MemberDetails from './MemberDetails';

class App extends Component {

  constructor() {
    super();
    this.state = { member:null, data: null, isDataPopulated : false };
  }

   async componentDidMount() {
     try {
      const response = await fetch(`https://yayjk.dev/api/members/allIds`);
       const json = await response.json();
       this.setState({ data: json["memberIdsList"]});
     } catch (error) {
       console.log(error);
     }
    
  }



  render(){

    var members= this.state.data;

    return (

      <div>
        <div className="nav bg-dark">
          {
            members && 
            members.map((member, index) => {                          
            return (
              <div className="p-2" key={index+1}>
                <button id={member['_id']} className=" btn btn-danger" onClick={() => this.setState({member:member['_id'] , isDataPopulated:true, data:null})}>
                  {member['_id']}
                </button>                       
              </div>
            );
            })
          }
        </div>
        {this.state.isDataPopulated && this.state.member && <MemberDetails memberId={this.state.member}></MemberDetails>}
      </div>
     
    );
  }
}

export default App;

