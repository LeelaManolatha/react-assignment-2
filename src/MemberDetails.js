import React,{Component} from 'react';

class MemberDetails extends Component {

    constructor() {
        super();
        this.state = { selectedMemberId: null, datad:null,datat:null, isDataPopulated:false};       
      }

      

      async componentDidMount() {
         try {
           const response = await fetch(`https://yayjk.dev/api/members/${this.props.memberId}`);
           const json = await response.json();
           this.setState({ selectedMemberId: this.props.memberId });
           this.setState({ datad: json['memberDetails'], datat: json['teamMembersId'],isDataPopulated:true });           
         } catch (error) {
           console.log(error);
         }        
    }

   

    async componentDidUpdate(prevProps, prevState){
        if(prevState.selectedMemberId !== this.state.selectedMemberId ){
           
           const response = await fetch(`https://yayjk.dev/api/members/${this.state.selectedMemberId}`);
           const json = await response.json();           
           this.setState({ datad: json['memberDetails'], datat: json['teamMembersId'],isDataPopulated:true});           
        }        
    }

    render(){
        
        var member = this.state.selectedMemberId;
        var memberDetails = this.state.datad ;
        var teamMembers = this.state.datat ;
        return (
            <div className="container-fluid">
                <div className="row">
                <div className="col bg-secondary"> 
                    <h2 className="text-white text-center">Team Members</h2>             
                    {
                    member && 
                    teamMembers &&
                    teamMembers.map((teamMember, index) => { 
                        return (
                            <div className="p-2 row  justify-content-center " id="" key={index+1}>
                                <button className="btn btn-warning col-4" onClick={()=>{
                                    this.setState({selectedMemberId:teamMember['_id'], isDataPopulated:false, datad:null})
                                    }}>
                                    {teamMember["_id"]}
                                </button><br/>
                            </div>                    
                        );
                    })
                    }
                    
                </div>

                <div className="col bg-light">
                <h2 className="text-center">Member Details</h2>
                    {
                    this.state.isDataPopulated  && member && 
                    memberDetails &&
                    
                    Object.keys(memberDetails).map((key,index)=>{
                        if(key==="dateOfJoining" && (key="Date Of Joining")){                            
                            var month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                            memberDetails[key]=new Date(memberDetails["dateOfJoining"]);                            
                            memberDetails[key]=month[memberDetails[key].getMonth()]+" "+memberDetails[key].getDate()+","+memberDetails[key].getFullYear();
                                                        
                        }
                        else if(key==="__v"){
                            return ;
                        }                        
                        return (
                            <div className="row p-1" id={index+1}>
                                <div className="col ">
                                    <b>{key.charAt(0).toUpperCase() + key.slice(1)}</b>
                                </div>
                                <div className="col ">
                                    {memberDetails[key]}
                                </div>

                            </div>                    
                        );
                    })                
                    }
                </div>
             </div>
            </div>
        );
    }
}

export default MemberDetails;



    












